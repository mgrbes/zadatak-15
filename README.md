# Zadatak 15
**Zadatak 15
Opis aplikacije**


Zadatak15 je android aplikacija koja omogućuje vođenje administracije zaposlenika i zadataka. Aplikacija omogućuje logiranje zaposlenika, kreiranje zadataka, ispis detaila pojedinog zadatka, dodavanje novih zaposlenika te brisanje zadataka i zaposlenika.
Kod svakog logiranog usera moguće je dodavati zadatak koji se odma dodijeli njemu.
Dodavanje zadataka i zaposlenika izvrsava se preko input fieldova u android studio koji je kasnije salje na server te ovisno o ruti on izvrsava određeni SQL query. Ovisno o statusu zadatka u android studio je napravljeno da se slika ažurira.
Aplikacija ima tri vrste korisnika: Admine, usere i superusere. Admini imaju pristup
svim sadržajima u aplikaciji i imaju pristup u uklanjanje taskova i zaposlenika te njihovo dodavanje.
Useri se logiraju u aplikaciju kroz login sučelje i imaju
mogućnost dodavati zadatak. 
Na početnoj stranici aplikacije vidi se popis svih zadataka od logiranog usera, ako je u pitanju Admin on dobiva mogucnosti brisanja zadatka i usera.


**Epic 1: User vidi svoje zadatke i može kreirati novi zadatak**


**S1.1**
Kao User, kada pristupim HOMEPAGE stranici, mogu kreirati novi zadataka pritiskom na odgovarajuce dugme

Need: Naziv zadatka, opis, trenutni status, kompleksnost, id zaposlenika



**S1-2**
Kao User, kada pristupim HOMEPAGE stranici, mogu vidjeti postojeće zadatke

Need: id zaposlenika




**Epic 2: Admini mogu brisati zaposlenike i zadatke**


**S2**
Kao Admin, kada pristupim HOMEPAGE stranici, trebam moći
brisati zaposlenike i brisati taskove
Acceptance criteria:

- Admin se može ulogirati sa danim korisničkim podacima
- Admin može obrisati svakog zaposlenika
- Admin može obrisati svaki zadatak
- Admin bi trebao moći mjenjati detalje o pojedinom zadatku te zaposleniku







